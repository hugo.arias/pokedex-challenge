import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { PokemonComponent } from './components/pokemon/pokemon.component';
import { PokemonAvatarComponent } from './components/pokemon-avatar/pokemon-avatar.component';
import { PokemonStatsComponent } from './components/pokemon-stats/pokemon-stats.component';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [AppComponent, HeaderComponent, PokemonComponent, PokemonAvatarComponent, PokemonStatsComponent, FooterComponent],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
