import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pokemon-avatar',
  templateUrl: './pokemon-avatar.component.html',
  styleUrls: ['./pokemon-avatar.component.scss']
})
export class PokemonAvatarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
